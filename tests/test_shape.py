import os
import sys
import pytest
import numpy as np

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../src")))

from shape_builder import Shape


def test_perimeter():
    x = np.array([0, 1, 1, 0])
    y = np.array([0, 0, 1, 1])
    shape = Shape(x, y)
    assert shape.perimeter() == 4.0


def test_negatives():
    # To ensure negative coordinates do not result in negative perimeter or shapes
    x = np.array([0, -1, -1, 0])
    y = np.array([0, 0, 1, 1])
    shape = Shape(x, y)
    assert shape.perimeter() == 4.0
    x = np.array([0, -1, -1, 0])
    y = np.array([0, 0, -1, -1])
    shape = Shape(x, y)
    assert shape.perimeter() == 4.0
    x = np.array([0, -1, -1, 0])
    y = np.array([0, 0, 1, 1])
    shape = Shape(x, y)
    assert shape.area() == 1.0
    x = np.array([0, -1, -1, 0])
    y = np.array([0, 0, -1, -1])
    shape = Shape(x, y)
    assert shape.area() == 1.0


def test_area():
    x = np.array([0, 1, 1, 0])
    y = np.array([0, 0, 1, 1])
    shape = Shape(x, y)
    assert shape.area() == 1.0


def test_invalid_shape():
    x = np.array([0, 1, 1])
    y = np.array([0, 0, 1, 1])
    with pytest.raises(ValueError):
        _ = Shape(x, y)


def test_empty_shape():
    with pytest.raises(ValueError):
        _ = Shape([], [])
    x = np.array([])
    y = np.array([])
    with pytest.raises(ValueError):
        _ = Shape(x, y)


def test_incomplete_shape():
    # Minimum of 3 points can make a valid shape
    with pytest.raises(ValueError):
        _ = Shape([0], [0])

    with pytest.raises(ValueError):
        _ = Shape([0, 1], [1, 0])


def test_incomplete_colinear():
    # Minimum of 3 points which are not colinear
    with pytest.raises(ValueError):
        _ = Shape([0, 1, 2, 3], [0, 1, 2, 3])


def test_channel_perimeter():
    channel = Shape.channel(150, 75, 8, 6)
    assert channel.perimeter() == 588.0


def test_channel_area():
    channel = Shape.channel(150, 75, 8, 6)
    assert channel.area() == 2004.0


def test_channel_centroids():
    channel = Shape.channel(150, 75, 8, 6)
    A = channel.area()
    cx, cy = channel.centroid(A)
    assert cy == 75
    assert cx == pytest.approx(23.658, rel=1e-3)


def test_channel_second_moment_area():
    channel = Shape.channel(150, 75, 8, 6)
    channel.mesh()
    A = channel.area()
    cx, cy = channel.centroid(A)
    ix, iy = channel.second_moments_of_area(cx, cy, A)
    # Calculated via spreadsheet
    assert ix == pytest.approx(7258652.0, rel=1e-8)
    assert iy == pytest.approx(1137942.53892, rel=1e-8)


def test_Ibeam_perimeter():
    beam = Shape.I_beam(700, 250, 20, 10)
    assert beam.perimeter() == 2380


def test_Ibeam_area():
    beam = Shape.I_beam(700, 250, 20, 10)
    assert beam.area() == 16600


def test_Ibeam_centroids():
    beam = Shape.I_beam(700, 250, 20, 10)
    A = beam.area()
    cx, cy = beam.centroid(A)
    # From reference
    assert cx == 0
    assert cy == 350


def test_Ibeam_second_moment_area():
    beam = Shape.I_beam(700, 250, 20, 10)
    beam.mesh()
    A = beam.area()
    cx, cy = beam.centroid(A)
    ix, iy = beam.second_moments_of_area(cx, cy, A)
    # Tabulated values are provided to nearest 3 sig figures.
    assert ix == pytest.approx(1400e6, abs=5e6)
    assert iy == pytest.approx(52.1e6, abs=0.05e6)


def test_Ibeam_section_modulus():
    beam = Shape.I_beam(700, 250, 20, 10)
    beam.mesh()
    beam.calculate_properties()

    assert beam.properties.Zx1 == pytest.approx(beam.properties.Zx2, abs=1e-8)
    assert beam.properties.Zy1 == pytest.approx(beam.properties.Zy2, abs=1e-8)
    assert beam.properties.Zx1 == pytest.approx(3990e3, abs=5e6)
    assert beam.properties.Zy1 == pytest.approx(417e3, abs=0.5e6)


def test_Ibeam_radius_of_gyration():
    beam = Shape.I_beam(700, 250, 20, 10)
    beam.mesh()
    beam.calculate_properties()

    assert beam.properties.rx == pytest.approx(290, abs=1)
    assert beam.properties.ry == pytest.approx(56, abs=0.1)
