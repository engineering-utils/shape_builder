import plotly.graph_objs as go
import numpy as np
from numpy import ndarray
from scipy.spatial import Delaunay, distance
from typing import List, Tuple, Optional
from dataclasses import dataclass

try:
    from .utils import (
        are_points_collinear,
        dark_mode,
        is_point_inside_polygon,
        second_moment_area_tri,
        plot_mesh,
    )
except ImportError as e:
    print(f"{e}: If running file directly this error may be ignored")


@dataclass
class ShapeProperties:
    """
    A dataclass that represents the properties of a 2D shape.

    Attributes:
        P (float or None): The perimeter of the shape. Defaults to None.
        A (float or None): The area of the shape. Defaults to None.
        cx (float or None): The x-coordinate of the centroid of the shape, from the base. Defaults to None.
        cy (float or None): The y-coordinate of the centroid of the shape, from the left. Defaults to None.
        Ix (float or None): The second moment of area of the shape with respect to the x-axis. Defaults to None.
        Iy (float or None): The second moment of area of the shape with respect to the y-axis. Defaults to None.
        Zxt (float or None): The elastic section modulus about the x axis from the top. Defaults to None.
        Zxb (float or None): The elastic section modulus about the x axis from the bottom. Defaults to None.
        Zyt (float or None): The elastic section modulus about the y axis from the top. Defaults to None.
        Zyb (float or None): The elastic section modulus about the y axis from the bottom. Defaults to None.
        Sx (float or None): The plastic section modulus about the x axis. Defaults to None.
        Sy (float or None): The plastic section modulus about the y axis. Defaults to None.
    """

    P: Optional[float] = None
    A: Optional[float] = None
    cx: Optional[float] = None
    cy: Optional[float] = None
    Ix: Optional[float] = None
    Iy: Optional[float] = None
    Zx1: Optional[float] = None
    Zx2: Optional[float] = None
    Zy1: Optional[float] = None
    Zy2: Optional[float] = None
    Sx: Optional[float] = None
    Sy: Optional[float] = None
    rx: Optional[float] = None
    ry: Optional[float] = None

    def calculate_section_modulus(self, bounds: Tuple[float, float, float, float]):
        # y centroids are given as a distance from the bottom most fibre
        # x centroids are given as a distance from zero
        min_x, min_y, max_x, max_y = bounds
        self.Zx1 = self.Ix / (self.cy - min_y)
        self.Zy1 = self.Iy / (self.cx - min_x)
        self.Zx2 = self.Ix / (max_y - self.cy)
        self.Zy2 = self.Iy / (max_x - self.cx)

    def calculate_radius_of_gyration(self):
        self.rx = np.sqrt(self.Ix / self.A)
        self.ry = np.sqrt(self.Iy / self.A)


class Shape:

    """
    Represents a closed shape defined by a set of (x, y) coordinates.

    Args:
        x (list or array-like): The x-coordinates of the shape vertices.
        y (list or array-like): The y-coordinates of the shape vertices.

    Raises:
        ValueError: If lengths of x and y args are not equal
        ValueError: If zero-length arg is provided

    Attributes:
        x (ndarray): The x-coordinates of the shape vertices.
        y (ndarray): The y-coordinates of the shape vertices.
        simplices (Optional[ndarray]): The indices of the vertices that form each triangle in the Delaunay mesh of the shape,
            represented as a 2D numpy array of shape (n, 3), where n is the number of triangles. If the mesh has not been
            generated yet, this attribute is None.
        mesh_points (Optional[ndarray]): The points associated with the simplices represented as a 2D array of shape (n, 2),
            where n is the number of points. If the mesh has not been generated yet, this attribute is None.

    Examples:
        To create a square shape with vertices at (0,0), (1,0), (1,1), and (0,1), we can do:

        >>> x = [0, 1, 1, 0]
        >>> y = [0, 0, 1, 1]
        >>> shape = Shape(x, y)

    Methods:
        - trace_outline(): Returns a scatter plot of the outline of the shape.
        - trace_mesh(): Returns a scatter plot of the Delaunay mesh of the shape.
        - plot(use_mesh=True): Plots the shape using Plotly, including the Delaunay mesh if one is defined.
        - show_mesh(): Displays a plot of the Delaunay mesh of the shape using Plotly.
        - show(): Creates a plotly figure of the Shape object and calls `.show()` on the figure.
        - close(): Closes the current shape by connecting the last point to the first point.
        - is_closed(): Returns True if the shape is closed (i.e., the first and last vertices are the same), False otherwise.
        - bounds(): Returns the bounding box of the shape as a tuple (min_x, min_y, max_x, max_y).
        - subdivide(mesh_size): Subdivides the outline of the shape into smaller line segments with the desired length.
            Returns a new Shape object with the subdivided outline.

    """

    def __init__(self, x: List[float], y: List[float]):
        self.x = np.array(x)
        self.y = np.array(y)
        self.simplices = None  # This will be set by methods
        self.mesh_points: Optional[ndarray] = None
        self.properties: ShapeProperties = (
            ShapeProperties()
        )  # All values initialised to None

        # Ensure arrays are valid
        if len(self.x) != len(self.y):
            raise ValueError(
                f"Incompatible x ({len(self.x)}) and y ({len(self.y)}) arrays"
            )

        if len(self.x) < 3:
            raise ValueError(
                f"The arrays must have at least 3 points: {len(self.x)} points were provided"
            )

        if are_points_collinear(self.x, self.y):
            raise ValueError("Points provided are colinear")

        # Close the shape if it isn't
        if not self.is_closed():
            self.close()

    def trace_outline(self) -> go.Scatter:
        """Return a scatter plot of the outline of the shape.

        Returns:
            A plotly scatter plot object.
        """
        return go.Scatter(
            x=self.x,
            y=self.y,
            mode="markers+lines",
            fill="toself",
        )

    def plot(self, use_mesh=True) -> go.Figure:
        """
        Plots the shape using Plotly, including a mesh if one is defined.

        Args:
            use_mesh (bool, optional): Whether or not to include the mesh in the plot. Defaults to True.

        Returns:
            go.Figure: A Plotly figure object.
        """
        fig = go.Figure(data=self.trace_outline())

        fig.update_yaxes(scaleanchor="x", scaleratio=1)

        return fig

    def show_mesh(self) -> None:
        """
        Displays a plot of the Delaunay mesh of the shape using Plotly.

        Raises:
            ValueError: If no mesh has been generated for the shape.
        """
        if self.simplices is None:
            raise ValueError("No Mesh to Show")
        plot_mesh(self.mesh_points[:, 0], self.mesh_points[:, 1], self.simplices)

    def show(self):
        """
        Creates a plotly figure of the Shape object and calls `.show()` on the figure.
        """
        fig = self.plot()
        dark_mode(fig)
        fig.show()

    def close(self):
        """
        Close the current shape by connecting the last point to the first point.

        This method appends the first point of the shape to the end of the `x` and `y` arrays.

        Returns:
            None
        """
        self.x = np.append(self.x, self.x[0])
        self.y = np.append(self.y, self.y[0])

    def is_closed(self) -> bool:
        """
        Check if the shape is closed.

        Returns:
            bool: True if the shape is closed, False otherwise.
        """
        return self.x[0] == self.x[-1] and self.y[0] == self.y[-1]

    def bounds(self) -> Tuple[float, float, float, float]:
        """
        Returns the bounding box of the shape.

        Returns:
            (min_x, min_y, max_x, max_y): A tuple containing the minimum x and y coordinates and the maximum x and y
            coordinates of the shape.
        """
        min_x = np.min(self.x)
        max_x = np.max(self.x)
        min_y = np.min(self.y)
        max_y = np.max(self.y)

        return min_x, min_y, max_x, max_y

    def subdivide(self, mesh_size: float) -> "Shape":
        """Subdivides the outline of the shape into smaller line segments.

        Args:
            mesh_size (float): The desired length of the line segments.

        Returns:
            Shape: A new Shape object with the subdivided outline.

        Raises:
            ValueError: If the mesh size is larger than the dimensions of the shape.
        """
        # Check if the mesh size is larger than the dimensions of the shape
        min_x, min_y, max_x, max_y = self.bounds()
        if mesh_size >= max(max_x - min_x, max_y - min_y):
            raise ValueError("Mesh size is too large for the given shape dimensions.")

        # Subdivide the outline
        new_x, new_y = [], []
        for i in range(len(self.x) - 1):
            x1, y1 = self.x[i], self.y[i]
            x2, y2 = self.x[i + 1], self.y[i + 1]
            dist = np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            if dist <= mesh_size:
                new_x.append(x1)
                new_y.append(y1)
            else:
                num_segments = int(np.ceil(dist / mesh_size))
                dx = (x2 - x1) / num_segments
                dy = (y2 - y1) / num_segments
                for j in range(num_segments):
                    new_x.append(x1 + j * dx)
                    new_y.append(y1 + j * dy)
        new_x.append(self.x[-1])
        new_y.append(self.y[-1])

        return Shape(new_x, new_y)

    def mesh(self, mesh_size=None, inner_tolerance=0.5, subdivide=True):
        """Creates a Delaunay mesh of the Shape object.

        Args:
            mesh_size (float, optional): The size of the mesh grid cells, if none then the current points are triangulated. Defaults to None.
            min_ratio (float, optional): A ratio of the mesh_size representing the minimum distance from boundary points. Defaults to 0.5.
            subdivide (bool, optional):  If True will subdivide the boundary of the mesh using the mesh size. Defaults to True.

        Returns:
            None. The mesh is stored in self.simplices and can be accessed by calling the `simplices` attribute.

        Modifies:
            mesh_points: The points referenced by self.simplices
            simplices: The indices of the triangulated points

        Raises:
            ValueError: If the Shape object is not closed

        Notes:
            This method evenly distributes points inside the shape by creating a mesh grid based on `mesh_size`.
            Points within a certain radius of the boundary are removed, as well as points that are not inside
            the shape. The remaining points are then used to create a Delaunay mesh.
            When Checking if a point is inside the boundary, the points are checked against the original boudary points.

        Example:
            >>> shape = Shape([0, 1, 1, 0], [0, 0, 1, 1])
            >>> shape.mesh(mesh_size=0.05)
        """
        # If no mesh_size. Just triangulate current points
        if mesh_size is None:
            self.mesh_points = np.column_stack((self.x, self.y))

        else:
            # Otherwise new points are added for triangulation
            self._mesh_points_grid(mesh_size, subdivide)

        # Create Delaunay mesh
        tri = Delaunay(self.mesh_points)

        # Find the Centre of each triangle
        centres = np.mean(self.mesh_points[tri.simplices], axis=1)

        # Check if each triangle is inside the polygon (Uses un-subdivided shape)
        mask = np.array(
            list(
                map(
                    lambda p: is_point_inside_polygon(p[0], p[1], self.x, self.y),
                    centres,
                )
            )
        )

        self.simplices = tri.simplices[mask]

    def _mesh_points_grid(self, mesh_size, subdivide):
        """
        Generate a grid of mesh points for the shape.

        Args:
            mesh_size (float): The size of the mesh grid cells.
            subdivide (bool): A flag indicating whether to subdivide the shape before generating the mesh.

        Notes:
            This method generates a mesh grid of points within the shape's boundary, based on the given `mesh_size`.
            If `subdivide` is set to True, the shape boundary is subdivided using the `subdivide` method with the given `mesh_size`.
            The mesh points are then created by constructing a mesh grid within the bounding box of the shape and flattening it.
            Points within a radius of 0.5 * mesh_size of any boundary point are removed to avoid duplicate points.
            Finally, points outside of the shape are removed using the `is_point_inside_polygon` function.

        """
        if subdivide:
            new_mesh = self.subdivide(mesh_size)
            x = new_mesh.x
            y = new_mesh.y
        else:
            x = self.x
            y = self.y

        # Create a mesh grid
        x_range = np.arange(np.min(x), np.max(x), mesh_size)
        y_range = np.arange(np.min(y), np.max(y), mesh_size)
        x_grid, y_grid = np.meshgrid(x_range, y_range)

        # Flatten the mesh grid into a list of points
        new_points = np.column_stack((x_grid.ravel(), y_grid.ravel()))

        # Remove points within a radius of length 0.5*mesh_size of any boundary point
        boundary_points = np.column_stack((x, y))
        distances = distance.cdist(new_points, boundary_points)
        min_distances = np.min(distances, axis=1)
        new_points = new_points[min_distances > 0.5 * mesh_size]

        # Remove points outside of the shape
        mask = np.array(
            list(
                map(
                    lambda p: is_point_inside_polygon(p[0], p[1], self.x, self.y),
                    new_points,
                )
            )
        )

        new_points = new_points[mask]

        # Combine boundary points and new points
        points = np.vstack((boundary_points, new_points))

        self.mesh_points = points

    def perimeter(self) -> float:
        """
        Calculates the perimeter of the shape, which is the sum of distances between consecutive points
        along the boundary of the shape. The calculation uses the Euclidean distance formula.

        Returns:
            float: The perimeter of the shape.
        """
        # Assuming this is faster than using cdist as it avoids reshaping the arrays: untested however.
        dx = np.diff(self.x)
        dy = np.diff(self.y)
        distances = np.sqrt(dx**2 + dy**2)
        return np.sum(distances)

    def area(self) -> float:
        """
        Calculates the area of the shape using the shoelace formula, which works for any non-self-intersecting
        polygon, including concave and convex shapes.

        Returns:
            float: The area of the shape.

        Notes:
            No check is performed to see if the shape is self-intersecting. As with all items here it is the users
            repsponsibility to confirm these methods are appropriate.
        """
        return 0.5 * np.abs(
            np.dot(self.x, np.roll(self.y, 1)) - np.dot(self.y, np.roll(self.x, 1))
        )

    def centroid(self, A: float) -> Tuple[float, float]:
        """
        Calculates the centroid of the shape using the coordinates of the boundary points.
        Values are from the 0, 0 reference point.

        Args:
            A (float): the area of the shape. Can be provided as `self.area()`

        Returns:
            Tuple[float, float]: The centroid coordinates as a tuple (cx,cy).
        """
        x = self.x
        y = self.y
        cx = np.sum((x[:-1] + x[1:]) * (x[:-1] * y[1:] - x[1:] * y[:-1])) / (6 * A)
        cy = np.sum((y[:-1] + y[1:]) * (x[:-1] * y[1:] - x[1:] * y[:-1])) / (6 * A)
        return (cx, cy)

    def second_moments_of_area(self, cx, cy, A) -> Tuple[float, float]:
        """
        Calculates the second moments of area of the shape along the x and y axes.

        Args:
            cx (float): The x-coordinate of the centroid.
            cy (float): The y-coordinate of the centroid.
            A (float): The total area of the shape.

        Returns:
            Tuple[float, float]: The second moments of area of the shape along the x and y axes, respectively.
        """
        # Calculate the second moments of area
        Ix = 0.0
        Iy = 0.0

        if self.simplices is None:
            raise ValueError("Shape has not been meshed")

        for tri in self.simplices:
            # Get the points for triangle
            x = self.mesh_points[tri, 0]
            y = self.mesh_points[tri, 1]

            # Second moment of area of the triangle
            Ix_tri, Iy_tri = second_moment_area_tri(x, y, cx, cy)

            Ix += Ix_tri
            Iy += Iy_tri

        return Ix, Iy

    def calculate_properties(self) -> ShapeProperties:
        """
        Calculate the properties of the shape.

        Returns:
            ShapeProperties: An object containing the calculated properties of the shape.

        Notes:
            This method calculates various properties of the shape, including perimeter, area, centroid,
            and second moments of area. The calculations are based on the shape's mesh and its corresponding
            simplex data.

            If the shape is not meshed, the second moments of area are not calculated.
            However, the density of the mesh is not important for the determination of the geometric properties
        """
        self.properties.P = self.perimeter()
        self.properties.A = self.area()
        self.properties.cx, self.properties.cy = self.centroid(self.properties.A)
        # Can only calculate the remaining properties if the shape is meshed
        if self.simplices is not None:
            self.properties.Ix, self.properties.Iy = self.second_moments_of_area(
                self.properties.cx, self.properties.cy, self.properties.A
            )

        self.properties.calculate_section_modulus(self.bounds())
        self.properties.calculate_radius_of_gyration()

        return self.properties

    @classmethod
    def box(cls, b: float, d: float) -> "Shape":
        # fmt: off
        x = np.array([0, b, b, 0])
        y = np.array([0, 0, d, d])
        # fmt: on
        box = cls(x, y)
        box.close()

        return box

    @classmethod
    def channel(cls, d: float, b: float, t_f: float, t_w: float) -> "Shape":
        """
        Creates a channel shape with the specified dimensions.

        Args:
            d (float): The overall depth of the channel.
            b (float): The width of the flanges.
            t_f (float): The thickness of the flanges.
            t_w (float): The thickness of the web.

        Returns:
            Shape: A Shape object representing the channel.
        """
        # fmt: off
        x = np.array([0, b, b  , t_w, t_w    , b      , b, 0])
        y = np.array([0, 0, t_f, t_f, d - t_f, d - t_f, d, d])
        # fmt: on
        channel = cls(x, y)
        channel.close()
        return channel

    @classmethod
    def I_beam(cls, d: float, b: float, t_f: float, t_w: float):
        """
        Creates an I-beam shape.

        Args:
            d (float): The total height of the I-beam.
            b (float): The width of the flanges.
            t_f (float): The thickness of the flanges.
            t_w (float): The thickness of the web.

        Returns:
            Shape: An instance of the Shape class representing the I-beam.

        Example:
            >>> # Create a 700WB130
            >>> beam = Shape.I_beam(700, 250, 20, 10)
        """
        # fmt: off
        # Create one side.
        b /= 2
        t_w /= 2

        x = np.array([0, b, b  , t_w, t_w    , b      , b  , 0])
        y = np.array([0, 0, t_f, t_f, d - t_f, d - t_f, d  , d])
        # fmt: on
        # Mirror points and join together
        x = np.concatenate((x, -1 * x[::-1]))
        y = np.concatenate((y, y[::-1]))
        beam = cls(x, y)
        return beam


if __name__ == "__main__":
    from utils import (
        are_points_collinear,
        dark_mode,
        is_point_inside_polygon,
        second_moment_area_tri,
        plot_mesh,
    )

    # 700 WB 130
    beam = Shape.I_beam(700, 250, 20, 10)
    beam.mesh()
    print(f"{beam.calculate_properties()}")
    # beam.show_mesh()
