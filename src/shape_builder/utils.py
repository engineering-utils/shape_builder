import plotly.graph_objs as go
import plotly.io as pio
import numpy as np
from typing import List

# Used with the sorted function to create a new array
_INDICES_ = np.arange(3)


def dark_mode(fig):
    fig.update_layout(template=pio.templates["plotly_dark"])


def plot_mesh(x, y, simplices):
    """
    Displays a plot of the Delaunay mesh defined by the given points and simplices using Plotly.

    Args:
        x (np.ndarray): A 1D array of x-coordinates of the points in the mesh.
        y (np.ndarray): A 1D array of y-coordinates of the points in the mesh.
        simplices (np.ndarray): A 2D array of point indices representing the simplices in the mesh.

    Returns:
        None
    """
    traces = []
    for simplex in simplices:
        i, j, k = simplex
        trace = go.Scatter(
            x=[x[i], x[j], x[k], x[i]],
            y=[y[i], y[j], y[k], y[i]],
            mode="lines",
            line=dict(color="black"),
            fill="toself",
            fillcolor="orange",
            showlegend=False,
        )
        traces.append(trace)
    fig = go.Figure(data=traces)
    fig.update_yaxes(scaleanchor="x", scaleratio=1)
    dark_mode(fig)
    fig.show()


def is_point_inside_polygon(
    x: float, y: float, poly_x: List[float], poly_y: List[float]
) -> bool:
    """
    Check if a point (x,y) is inside a polygon defined by lists of vertices poly_x and poly_y.

    Args:
        x (float): The x-coordinate of the point to check.
        y (float): The y-coordinate of the point to check.
        poly_x (List[float]): A list of x-coordinates of the vertices of the polygon.
        poly_y (List[float]): A list of y-coordinates of the vertices of the polygon.

    Returns:
        bool: True if the point is inside the polygon, False otherwise.
    """
    n = len(poly_x)
    inside = False

    # Check each edge of the polygon
    p2x, p2y = poly_x[0], poly_y[0]
    for i in range(n + 1):
        p1x, p1y = p2x, p2y
        p2x, p2y = poly_x[i % n], poly_y[i % n]

        # Check if the point is above the minimum y-coordinate of the edge
        if y <= min(p1y, p2y):
            continue

        # Check if the point is below the maximum y-coordinate of the edge
        if y > max(p1y, p2y):
            continue

        # Check if the point is to the left of the maximum x-coordinate of the edge
        if x > max(p1x, p2x):
            continue

        # If the edge is vertical or the point is to the left of the intersection, toggle the inside flag
        if p1x == p2x:
            inside = not inside

    return inside


def second_moment_area_tri(
    x: List[float], y: List[float], cx_shp: float, cy_shp: float
):
    """
    Calculates the second moments of area (Ix and Iy) for a triangle with respect to its centroid.

    Args:
        x (List[float]): X-coordinates of the triangle's vertices.
        y (List[float]): Y-coordinates of the triangle's vertices.
        cx_shp (float): X-coordinate of the shape's centroid.
        cy_shp (float): Y-coordinate of the shape's centroid.

    Returns:
        tuple[float, float]: Second moments of area (Ix, Iy) for the triangle.

    Raises:
        AssertionError: If the length of the x and y lists is not equal to 3.

    """
    # Check Length
    assert len(x) == 3 and len(y) == 3

    # Get indices of the min, mid, and max values
    min_y, mid_y, max_y = _INDICES_[np.argsort(y)]

    h_top = y[max_y] - y[mid_y]
    h_bot = y[mid_y] - y[min_y]

    # Where a is the displacement of the outer most vertex from the base of the triangle
    ax_top = abs(x[max_y] - x[mid_y])
    ax_bot = abs(x[mid_y] - x[min_y])
    h = h_top + h_bot

    # Find the distance between the highest point in x and the intersection
    x_other = x[max_y] - x[min_y]
    new_x = x[max_y] - h_top * x_other / h
    b = abs(new_x - x[mid_y])

    A_top = b * h_top / 2
    A_bot = b * h_bot / 2

    x_top = (x[mid_y] + new_x + x[max_y]) / 3
    x_bot = (x[mid_y] + new_x + x[min_y]) / 3

    y_top = (2 * y[mid_y] + y[max_y]) / 3
    y_bot = (2 * y[mid_y] + y[min_y]) / 3

    Ix = (
        b / 36 * (h_top**3 + h_bot**3)
        + A_top * (cy_shp - y_top) ** 2
        + A_bot * (cy_shp - y_bot) ** 2
    )

    Iy_top = (b**3 * h_top - b**2 * h_top * ax_top + b * h_top * ax_top**2) / 36
    Iy_bot = (b**3 * h_bot - b**2 * h_bot * ax_bot + b * h_bot * ax_bot**2) / 36
    Iy = Iy_top + Iy_bot + A_top * (cx_shp - x_top) ** 2 + A_bot * (cx_shp - x_bot) ** 2

    return Ix, Iy


def are_points_collinear(x, y):
    """
    Check if a set of points defined by their x and y coordinates are collinear.

    Args:
        x (numpy.ndarray): Array of x-coordinates of the points.
        y (numpy.ndarray): Array of y-coordinates of the points.

    Returns:
        bool: True if the points are collinear, False otherwise.

    Raises:
        ValueError: If the input arrays `x` and `y` have different lengths.

    Examples:
        >>> x = np.array([0, 1, 2])
        >>> y = np.array([0, 1, 2])
        >>> are_points_collinear(x, y)
        True

        >>> x = np.array([0, 1, 2])
        >>> y = np.array([0, 2, 4])
        >>> are_points_collinear(x, y)
        False
    """
    # Calculate the differences between consecutive points
    dx = np.diff(x)
    dy = np.diff(y)

    # Ensures no division by zero
    # If any value is zero, then check if all values are zero
    if np.any(dy == 0):
        return np.allclose(dy, 0)

    # Check if the differences are proportional (collinear)
    return np.allclose(dx / dy, dx[0] / dy[0])
